"""
Mask R-CNN
Train on the nuclei segmentation dataset from the
Kaggle 2018 Data Science Bowl
https://www.kaggle.com/c/data-science-bowl-2018/

Licensed under the MIT License (see LICENSE for details)
Written by Waleed Abdulla

------------------------------------------------------------

Usage: import the module (see Jupyter notebooks for examples), or run from
       the command line as such:

    # Train a new model starting from ImageNet weights
    python3 nucleus.py train --dataset=/path/to/dataset --subset=train --weights=imagenet

    # Train a new model starting from specific weights file
    python3 nucleus.py train --dataset=/path/to/dataset --subset=train --weights=/path/to/weights.h5

    # Resume training a model that you had trained earlier
    python3 nucleus.py train --dataset=/path/to/dataset --subset=train --weights=last

    # Generate submission file
    python3 nucleus.py detect --dataset=/path/to/dataset --subset=train --weights=<last or /path/to/weights.h5>
"""

# Set matplotlib backend
# This has to be done before other importa that might
# set it, but only if we're running in script mode
# rather than being imported.
if __name__ == '__main__':
    import matplotlib
    # Agg backend runs without a display
    matplotlib.use('Agg')
    import matplotlib.pyplot as plt

import os
import sys
import json
import datetime
import numpy as np
import skimage.io
from imgaug import augmenters as iaa

# Root directory of the project
ROOT_DIR = os.path.abspath("../../")

# Import Mask RCNN
sys.path.append(ROOT_DIR)  # To find local version of the library
from mrcnn.config import Config
from mrcnn import utils
from mrcnn import model as modellib
from mrcnn import visualize
import numpy as np
import scipy.misc as misc
import os
import csv
import nrrd
import re
from collections import Counter

# Path to trained weights file
COCO_WEIGHTS_PATH = os.path.join(ROOT_DIR, "mask_rcnn_coco.h5")

# Directory to save logs and model checkpoints, if not provided
# through the command line argument --logs
DEFAULT_LOGS_DIR = os.path.join(ROOT_DIR, "logs")

# Results directory
# Save submission files here
RESULTS_DIR = os.path.join(ROOT_DIR, "results/nucleus/")

# The dataset doesn't have a standard train/val split, so I picked
# a variety of images to surve as a validation set.
VAL_IMAGE_IDS = [
    'Case_4',
    'Case_11',
    'Case_19',
    'Case_24',
    'Case_35',
    'Case_43',
    'Case_57',
    'Case_67',
    'Case_97',
    'Case_55'
]


############################################################
#  Configurations
############################################################

class NucleusConfig(Config):
    """Configuration for training on the nucleus segmentation dataset."""
    # Give the configuration a recognizable name
    NAME = "nucleus" #TODO: Change this to Covid if possible.

    # Adjust depending on your GPU memory
    IMAGES_PER_GPU = 1

    # Number of classes (including background)
    NUM_CLASSES = 1 + 3  # Background + nucleus

    # Number of training and validation steps per epoch
    STEPS_PER_EPOCH = (657 - len(VAL_IMAGE_IDS)) // IMAGES_PER_GPU
    VALIDATION_STEPS = max(1, len(VAL_IMAGE_IDS) // IMAGES_PER_GPU)

    # Don't exclude based on confidence. Since we have two classes
    # then 0.5 is the minimum anyway as it picks between nucleus and BG
    DETECTION_MIN_CONFIDENCE = 0.9

    # Backbone network architecture
    # Supported values are: resnet50, resnet101
    BACKBONE = "resnet50"

    # Input image resizing
    # Random crops of size 512x512
    IMAGE_RESIZE_MODE = "crop"
    IMAGE_MIN_DIM = 512
    IMAGE_MAX_DIM = 512
    IMAGE_MIN_SCALE = 2.0

    # Length of square anchor side in pixels
    RPN_ANCHOR_SCALES = (8, 16, 32, 64, 128)

    # ROIs kept after non-maximum supression (training and inference)
    POST_NMS_ROIS_TRAINING = 1000
    POST_NMS_ROIS_INFERENCE = 2000

    # Non-max suppression threshold to filter RPN proposals.
    # You can increase this during training to generate more propsals.
    RPN_NMS_THRESHOLD = 0.9

    # How many anchors per image to use for RPN training
    RPN_TRAIN_ANCHORS_PER_IMAGE = 64

    # Image mean (RGB)
    MEAN_PIXEL = np.array([43.53, 39.56, 48.22])

    # If enabled, resizes instance masks to a smaller size to reduce
    # memory load. Recommended when using high-resolution images.
    USE_MINI_MASK = True
    MINI_MASK_SHAPE = (56, 56)  # (height, width) of the mini-mask

    # Number of ROIs per image to feed to classifier/mask heads
    # The Mask RCNN paper uses 512 but often the RPN doesn't generate
    # enough positive proposals to fill this and keep a positive:negative
    # ratio of 1:3. You can increase the number of proposals by adjusting
    # the RPN NMS threshold.
    TRAIN_ROIS_PER_IMAGE = 128

    # Maximum number of ground truth instances to use in one image
    MAX_GT_INSTANCES = 200 #200 original

    # Max number of final detections per image
    DETECTION_MAX_INSTANCES = 10 #200 original


class NucleusInferenceConfig(NucleusConfig):
    # Set batch size to 1 to run one image at a time
    GPU_COUNT = 1
    IMAGES_PER_GPU = 1
    # Don't resize imager for inferencing
    IMAGE_RESIZE_MODE = "pad64"
    # Non-max suppression threshold to filter RPN proposals.
    # You can increase this during training to generate more propsals.
    RPN_NMS_THRESHOLD = 0.7


############################################################
#  Dataset
############################################################

class NucleusDataset(utils.Dataset):

    #This is a temp modification to test functionality, it will not be retained later.

    def load_nucleus(self, dataset_dir, subset):
        """Load a subset of the nuclei dataset.

        dataset_dir: Root directory of the dataset
        subset: Subset to load. Either the name of the sub-directory,
                such as stage1_train, stage1_test, ...etc. or, one of:
                * train: stage1_train excluding validation images
                * val: validation images from VAL_IMAGE_IDS
        """
        # Add classes. We have one class.
        # Naming the dataset nucleus, and the class nucleus
        self.add_class("nucleus", 1, "GGO")
        self.add_class("nucleus", 2, "Consolidation")
        self.add_class("nucleus", 3, "PE")
        # Which subset?
        # "val": use hard-coded list above
        # "train": use data from stage1_train minus the hard-coded list above
        # else: use the data from the specified sub-directory
        assert subset in ["train", "val", "stage1_train", "stage1_test", "stage2_test"]
        subset_dir = "stage1_train" if subset in ["train", "val"] else subset
        dataset_dir = os.path.join(dataset_dir, subset_dir)
        if subset == "val":
            image_ids = VAL_IMAGE_IDS
        else:
            # Get image ids from directory names
            image_ids = next(os.walk(dataset_dir))[1]
            if subset == "train":
                image_ids = list(set(image_ids) - set(VAL_IMAGE_IDS))

        # Add images
        for image_id in image_ids:
            self.add_image(
                "nucleus",
                image_id=image_id,
                path=os.path.join(dataset_dir, image_id, "images/{}.png".format(image_id)))

    def load_mask(self, image_id):
        """Generate instance masks for an image.
       Returns:
        masks: A bool array of shape [height, width, instance count] with
            one mask per instance.
        class_ids: a 1D array of class IDs of the instance masks.
        """
        info = self.image_info[image_id]
        # Get mask directory from image path
        mask_dir = os.path.join(os.path.dirname(os.path.dirname(info['path'])), "masks")

        # Read mask files from .png image
        mask = []
        class_id = []
        curr_image = 0

        for f in next(os.walk(mask_dir))[2]:
            if f.endswith(".png"):
                m = skimage.io.imread(os.path.join(mask_dir, f)).astype(np.bool)
                mask.append(m)
            if "GGO" in f:
                class_id.append(1)
            elif "Cons" in f:
                class_id.append(2)
            elif "PE" in f:
                class_id.append(3)
            else:
                print("Unable to process file!")
                exit(2)
            curr_image = curr_image + 1

        mask = np.stack(mask, axis=-1)
        class_id = np.stack(class_id, axis=-1)
        # Return mask, and array of class IDs of each instance. Since we have
        # one class ID, we return an array of ones
        return mask, class_id

    def image_reference(self, image_id):
        """Return the path of the image."""
        info = self.image_info[image_id]
        if info["source"] == "nucleus":
            return info["id"]
        else:
            super(self.__class__, self).image_reference(image_id)


############################################################
#  Training
############################################################

def train(model, dataset_dir, subset):
    """Train the model."""
    # Training dataset.
    dataset_train = NucleusDataset()
    dataset_train.load_nucleus(dataset_dir, subset)
    dataset_train.prepare()

    # Validation dataset
    dataset_val = NucleusDataset()
    dataset_val.load_nucleus(dataset_dir, "val")
    dataset_val.prepare()

    # Image augmentation
    # http://imgaug.readthedocs.io/en/latest/source/augmenters.html
    augmentation = iaa.SomeOf((0, 2), [
        iaa.Fliplr(0.5),
        iaa.Flipud(0.5),
        iaa.OneOf([iaa.Affine(rotate=90),
                   iaa.Affine(rotate=180),
                   iaa.Affine(rotate=270)]),
        iaa.Multiply((0.8, 1.5)),
        iaa.GaussianBlur(sigma=(0.0, 5.0))
    ])

    # *** This training schedule is an example. Update to your needs ***

    # If starting from imagenet, train heads only for a bit
    # since they have random weights
    print("Train network heads")
    model.train(dataset_train, dataset_val,
                learning_rate=config.LEARNING_RATE,
                epochs=20,
                augmentation=augmentation,
                layers='heads')

    print("Train all layers")
    model.train(dataset_train, dataset_val,
                learning_rate=config.LEARNING_RATE,
                epochs=40,
                augmentation=augmentation,
                layers='all')


############################################################
#  RLE Encoding
############################################################

def rle_encode(mask):
    """Encodes a mask in Run Length Encoding (RLE).
    Returns a string of space-separated values.
    """
    assert mask.ndim == 2, "Mask must be of shape [Height, Width]"
    # Flatten it column wise
    m = mask.T.flatten()
    # Compute gradient. Equals 1 or -1 at transition points
    g = np.diff(np.concatenate([[0], m, [0]]), n=1)
    # 1-based indicies of transition points (where gradient != 0)
    rle = np.where(g != 0)[0].reshape([-1, 2]) + 1
    # Convert second index in each pair to lenth
    rle[:, 1] = rle[:, 1] - rle[:, 0]
    return " ".join(map(str, rle.flatten()))


def rle_decode(rle, shape):
    """Decodes an RLE encoded list of space separated
    numbers and returns a binary mask."""
    rle = list(map(int, rle.split()))
    rle = np.array(rle, dtype=np.int32).reshape([-1, 2])
    rle[:, 1] += rle[:, 0]
    rle -= 1
    mask = np.zeros([shape[0] * shape[1]], np.bool)
    for s, e in rle:
        assert 0 <= s < mask.shape[0]
        assert 1 <= e <= mask.shape[0], "shape: {}  s {}  e {}".format(shape, s, e)
        mask[s:e] = 1
    # Reshape and transpose
    mask = mask.reshape([shape[1], shape[0]]).T
    return mask


def mask_to_rle(image_id, mask, scores):
    "Encodes instance masks to submission format."
    assert mask.ndim == 3, "Mask must be [H, W, count]"
    # If mask is empty, return line with image ID only
    if mask.shape[-1] == 0:
        return "{},".format(image_id)
    # Remove mask overlaps
    # Multiply each instance mask by its score order
    # then take the maximum across the last dimension
    order = np.argsort(scores)[::-1] + 1  # 1-based descending
    mask = np.max(mask * np.reshape(order, [1, 1, -1]), -1)
    # Loop over instance masks
    lines = []
    for o in order:
        m = np.where(mask == o, 1, 0)
        # Skip if empty
        if m.sum() == 0.0:
            continue
        rle = rle_encode(m)
        lines.append("{}, {}".format(image_id, rle))
    return "\n".join(lines)

############################################################
#  Custom UM Functions
############################################################

def exportPackageMasks(dataset, masks):
    """Reconstructs masks to make them fit for export."""
    bladder = []
    femur = []
    prostate = []
    gm = []
    muscle = []
    rows = len(masks[-1])
    cols = len(masks[0][-1])


    return 0

def findMatchClass(tgtClass,class_ids):
    """Finds the index of matching class_ids and returns them for processing."""
    buffer = np.where(class_ids == tgtClass)[0]
    if len(buffer) > 1:
        return buffer
    else:
        return "Single"

def initSliceAverageCSV(outputPath, contourSet):
    """Initialize output CSV for writing results."""
    if contourSet == "Prostate":
            csvfile = open(outputPath, 'w')
            writer = csv.writer(csvfile, dialect='unix', delimiter=',',
                                quotechar='|', quoting=csv.QUOTE_MINIMAL, )
            writer.writerow(['Image ID','Prostate', 'GM', 'Muscle', 'Bladder', 'Femur'])
    elif contourSet == "Pancreas": #TODO: Make this function supported
        print("WARNING: Pancreas volume averages are not yet supported!")
        exit(1)
    elif contourSet == "Cervix": #TODO: Make this function supported
        print("WARNING: Cervix volume averages are not yet supported!")
        exit(1)
    else:
        print("Cannot find class " + contourSet + " to initiate output csv!")
        exit(1)
    return csvfile, writer

def maskImage(image, mask):
    """Apply a mask to the image and return the output."""
    basicMask = image.T * mask.T
    output = np.ma.masked_equal(basicMask, 0)
    return output

def prostateMaskImageToClass(image, prostateMask, gmMask, muscleMask, bladderMask, femurMask):
    """Generate masks for all prostate classes in a single function and return in order."""
    maskedProstate = maskImage(image, prostateMask)
    maskedGM = maskImage(image, gmMask)
    maskedMuscle = maskImage(image, muscleMask)
    maskedBladder = maskImage (image, bladderMask)
    maskedFemur = maskImage(image, femurMask)
    return maskedProstate, maskedGM, maskedMuscle, maskedBladder, maskedFemur


def prostateAvgsToFile(csvrows, imageName, maskedProstate, maskedGM, maskedMuscle, maskedBladder, maskedFemur):
    """Compile the averages of each of the masked prostate images and writes them to the results file."""
    prostateAvg = np.average(maskedProstate)
    gmAvg = np.average(maskedGM)
    muscleAvg = np.average(maskedMuscle)
    bladderAvg = np.average(maskedBladder)
    femurAvg = np.average(maskedFemur)
    csvrows.append([imageName, prostateAvg, gmAvg, muscleAvg, bladderAvg, femurAvg])

class StreamVolumeContainer:
    """A structure which allows streaming 2D image arrays to be stored and reconstructed into a single 3D array."""
    data = []

    def __init__(self, xdim, ydim, zdim):
        self.data = np.zeros((xdim, ydim, zdim),dtype=np.float64)
    def reInitialize(self,ydim, xdim, zdim):
        self.data = []
        self.data = np.zeros((xdim, ydim, zdim), dtype=np.float64)
    def getDims(self):
        ydim = self.data.shape[1]
        xdim = self.data.shape[0]
        zdim = self.data.shape[2]
        return xdim, ydim, zdim
    def storeSlice(self, img):
        """Locates the first empty slice in the storage volume and save the slice there."""
        for i in range(len(self.data[0,0,:])):
            if np.average(self.data[:, :, i]) == 0:
                self.data[:, :, i] = img.astype(int)
                break
            else:
                pass
    def storeSliceSpecific(self, img, tgtSlice):
        """Stores the image array into the 'slot' specified by the user."""
        self.data[:, :, tgtSlice] = img.astype(int)
    def storeVolume(self, vol):
        """Stores a volume into the container if the dimensions match."""
        if self.data.shape == vol.shape:
            self.data = vol
        else:
            print("StreamVolumeContainer DIMS MISMATCH!")
            exit(1)
    def getData(self):
        return self.data
    def maskWith(self, otherSwc):
        """Mask the referenced volume with another outside volume."""
        xdim, ydim, zdim = self.getDims()
        result = StreamVolumeContainer(xdim,ydim,zdim)
        result.data = self.data.T * otherSwc.data.T
        return result
    def getDataAverage(self):
        #out = np.average(self.data.T)
        out = self.data
        out[out == 0] = np.nan
        res = np.nanmean(out)
        return res

def getUniqueCases(dataset_dir, subset):
    """
    Finds the number of unique cases in the specified path and the number of times (corresponding to slices) each
    item occurs.
    :param dataset_dir: The dataset dir as specified in the MRCNN argument line.
    :param subset: The subset dir as specified in the MRCNN argument line.
    :return: A dictionary containing the Case and the number of times it occurs.
    """
    path = dataset_dir + '/' + subset
    # Now we search inside the path for the number of unique files for each "case"
    subset_contents = os.listdir(path)
    # Filtering makes sure that we only get the folders we're expecting (just in case)
    reg = re.compile("(^\w+-\d{4})")
    subset_filtered = list(filter(reg.match, subset_contents))
    # We need to remove the ends of these strings to get something countable. Removing the _AX_#### of the string.
    for i, _ in enumerate(subset_filtered):
        subset_filtered[i] = subset_filtered[i][:-8]
    # We now have a list of just repeating 'Case-####' directories. We want to see how many times each repeats.
    cases = Counter(subset_filtered)

    return cases

def getUniqueCasesPancreas(dataset_dir, subset):
    """
    Finds the number of unique cases in the specified path and the number of times (corresponding to slices) each
    item occurs.
    :param dataset_dir: The dataset dir as specified in the MRCNN argument line.
    :param subset: The subset dir as specified in the MRCNN argument line.
    :return: A dictionary containing the Case and the number of times it occurs.
    """
    path = dataset_dir + '/' + subset
    # Now we search inside the path for the number of unique files for each "case"
    subset_contents = os.listdir(path)
    # Filtering makes sure that we only get the folders we're expecting (just in case)
    reg = re.compile("(^\w+-\d{4})")
    subset_filtered = list(filter(reg.match, subset_contents))
    # We need to remove the ends of these strings to get something countable. Removing the _AX_#### of the string.
    for i, _ in enumerate(subset_filtered):
        subset_filtered[i] = subset_filtered[i].split('_')[0]
    # We now have a list of just repeating 'Case-####' directories. We want to see how many times each repeats.
    cases = Counter(subset_filtered)

    return cases

def getOccurances(dict, case):
    """
    This is a simple wrapper for code legibility to be used with getUniqueCases.
    :param dict: The target dictionary
    :param case: The particular element of the dictionary you wish to read.
    :return: An int value of the dictionary at that location.
    """
    return dict[case]

class CaseIndex:
    """
    A data structure to add linked-list functionality to our case dictionary.
    """
    cases = {}
    keys = []
    ptr = []

    def __init__(self, cases):
        self.cases = cases
        self.regenerateKeys()

    def isLastEntry(self):
        if self.ptr == len(self.keys) - 1:
            return True
        else:
            return False

    def regenerateKeys(self):
        self.keys = list(self.cases.keys())

    def initPointer(self):
        self.ptr = 0

    def next(self):
        if self.isLastEntry() == False:
            self.ptr = self.ptr + 1
        else:
            print("ERROR: Attempting to iterate pointer past the end of list!")
            exit(3)

    def prev(self):
        if self.ptr > 1:
            self.ptr = self.ptr - 1
        else:
            print("ERROR: Unable to reverse pointer below a value of 1!")
            exit(3)

    def getDataAt(self, case):
        return self.cases[case]

    def getDataAtPointer(self):
        tgt = self.keys[self.ptr]
        return self.cases[tgt]

    def getKeyAtPointer(self):
        return self.keys[self.ptr]

def purgeSvcDataNormProstate(cIndex, svcProstate, svcMuscle, svcBladder, svcGM, svcFemur):
    """
    Forcibly save the contents of the Normalization contours regardless of current state.
    :param cIndex: a CaseIndex struct
    :param svcProstate: the Prostate contour StreamVolumeContainer
    :param svcMuscle: Muscle SVC
    :param svcBladder: Bladder SVC
    :param svcGM: GM SVC
    :param svcFemur: Femur SVC
    :return: NULL
    """
    path = "D:/MaskTest/" + str(cIndex.keys[cIndex.ptr])
    try:
        os.mkdir(path)
    except:
        print("Unable to make directory for contour nrrd!")
    try:
        nrrd.write(path + "/ctr_pro.nrrd", svcProstate.getData())
        nrrd.write(path + "/ctr_Muscle.nrrd", svcMuscle.getData())
        nrrd.write(path + "/ctr_Bladder.nrrd", svcBladder.getData())
        nrrd.write(path + "/ctr_GM.nrrd", svcGM.getData())
        nrrd.write(path + "/ctr_Femur.nrrd", svcFemur.getData())
    except:
        print("Unable to write contour nrrd to file!")
        exit(3)

def purgeSvcDataNormPancreas(cIndex, svcSkin, svcLiver, svcKidney, svcSpine, svcBowel, svcStomach, svcDuodenum,
                             svcPancreas, svcGTV):
    """
    Forcibly save the contents of the Normalization contours regardless of current state.
    :param cIndex: a CaseIndex struct
    :param svcProstate: the Prostate contour StreamVolumeContainer
    :param svcMuscle: Muscle SVC
    :param svcBladder: Bladder SVC
    :param svcGM: GM SVC
    :param svcFemur: Femur SVC
    :return: NULL
    """
    path = "D:/MaskTest/" + str(cIndex.keys[cIndex.ptr])
    try:
        os.mkdir(path)
    except:
        print("Unable to make directory for contour nrrd!")
    try:
        nrrd.write(path + "/ctr_Skin.nrrd", svcSkin.getData())
        nrrd.write(path + "/ctr_Liver.nrrd", svcLiver.getData())
        nrrd.write(path + "/ctr_Kidney.nrrd", svcKidney.getData())
        nrrd.write(path + "/ctr_SpinalCord.nrrd", svcSpine.getData())
        nrrd.write(path + "/ctr_Bowel.nrrd", svcBowel.getData())
        nrrd.write(path + "/ctr_Stomach.nrrd", svcStomach.getData())
        nrrd.write(path + "/ctr_Duodenum.nrrd", svcDuodenum.getData())
        nrrd.write(path + "/ctr_Pancreas.nrrd", svcPancreas.getData())
        nrrd.write(path + "/ctr_GTV.nrrd", svcGTV.getData())
    except:
        print("Unable to write contour nrrd to file!")
        exit(3)

    # self.add_class("nucleus", 1, "Skin")
    # self.add_class("nucleus", 2, "Liver")
    # self.add_class("nucleus", 3, "Kidney")
    # self.add_class("nucleus", 4, "SpinalCord")
    # self.add_class("nucleus", 5, "Bowel")
    # self.add_class("nucleus", 6, "Stomach")
    # self.add_class("nucleus", 7, "Duodenum")
    # self.add_class("nucleus", 8, "Pancreas")
    # self.add_class("nucleus", 9, "GTV")





############################################################
#  Detection
############################################################

def detect(model, dataset_dir, subset):
    """Run detection on images in the given directory."""
    print("Running on {}".format(dataset_dir))

    # Create directory
    if not os.path.exists(RESULTS_DIR):
        os.makedirs(RESULTS_DIR)
    submit_dir = "submit_{:%Y%m%dT%H%M%S}".format(datetime.datetime.now())
    submit_dir = os.path.join(RESULTS_DIR, submit_dir)
    os.makedirs(submit_dir)

    # Read dataset
    dataset = NucleusDataset()
    dataset.load_nucleus(dataset_dir, subset)
    dataset.prepare()
    # Load over images
    exampleNumber = 0
    submission = []

    for image_id in dataset.image_ids:
        # Load image and run detection
        image = dataset.load_image(image_id)
        # Detect objects
        r = model.detect([image], verbose=0)[0]
        print("Injection point for mask saving!!!!!!")
        print(exampleNumber)
        classLegend = { #Set up a legend to interpret classes
            1: "GGO",
            2: "Consolidation",
            3: "PE",
        }
        classIds = r['class_ids'] #Retrieve and set up structures for holding data and sourcing data
        multiGGO = findMatchClass(1,classIds)
        multiConsolidation = findMatchClass(2, classIds)
        multiPE = findMatchClass(3, classIds)
        masks = r['masks']
        for ii,currRow in enumerate(masks):   #Process the pixel data into lists by class.
          for classCount in range(0,len(classIds)):
                if ii == 0:
                  ggo = np.zeros((masks.shape[1], masks.shape[0]))
                  consolidation = np.zeros((masks.shape[1], masks.shape[0]))
                  pe = np.zeros((masks.shape[1], masks.shape[0]))
                currClass = classLegend[classIds[classCount]] #TODO: Refactoring: Clean all of this up into neat single functions.
                if currClass == "GGO":
                    if multiGGO != "Single":
                        ggo[:, ii] = currRow[:, multiGGO[0]].astype(int) + currRow[:, multiGGO[1]].astype(int) #TODO: Need to see number of anticipated detections.
                    else:
                        ggo[:, ii] = currRow[:, classCount].astype(int)
                elif currClass == "Consolidation":
                    if multiConsolidation != "Single":
                        consolidation[:,ii] = currRow[:, multiConsolidation[0]].astype(int) + currRow[:, multiConsolidation[1]].astype(int)
                    else:
                        consolidation[:, ii] = currRow[:, classCount].astype(int)
                elif currClass == "PE":
                    if multiPE != "Single":
                        pe[:,ii] = currRow[:, multiPE[0]].astype(int) + currRow[:, multiPE[1]].astype(int)
                    else:
                        pe[:, ii] = currRow[:, classCount].astype(int)
        os.makedirs("D:/MaskTest/" + dataset.image_info[image_id]["id"] +"/")
        if len(classIds) == 0:
            #If we have no detections, we move on.
            print("No detections on this image!")
            continue
        np.clip(ggo.T, 0, 1, out=ggo.T)
        np.clip(consolidation.T, 0, 1, out=consolidation.T)
        np.clip(pe.T, 0, 1, out=pe.T)

        if np.amax(ggo.T) > 0:
            misc.toimage(ggo.T * 255).save("D:/MaskTest/" + dataset.image_info[image_id]["id"] +"/GGO.png")
            #svcSkin.storeSliceSpecific(np.rot90(np.fliplr(skin.T),1), tgtSlice)
        if np.amax(pe.T) > 0:
            misc.toimage(pe.T * 255).save("D:/MaskTest/" + dataset.image_info[image_id]["id"] +"/PE.png")
            #svcLiver.storeSliceSpecific(np.rot90(np.fliplr(liver.T), 1), tgtSlice)
        if np.amax(consolidation.T) > 0:
            misc.toimage(consolidation.T * 255).save("D:/MaskTest/" + dataset.image_info[image_id]["id"] + "/Consolidation.png")
            #svcKidney.storeSliceSpecific(np.rot90(np.fliplr(kid.T),1), tgtSlice)
        exampleNumber = exampleNumber + 1
        # Encode image to RLE. Returns a string of multiple lines
        source_id = dataset.image_info[image_id]["id"]
        rle = mask_to_rle(source_id, r["masks"], r["scores"])
        submission.append(rle)
        # Save image with masks
        visualize.display_instances(
            image, r['rois'], r['masks'], r['class_ids'],
            dataset.class_names, r['scores'],
            show_bbox=False, show_mask=False,
            title="Predictions")
        plt.savefig("{}/{}.png".format(submit_dir, dataset.image_info[image_id]["id"]))


############################################################
#  Command Line
############################################################

if __name__ == '__main__':
    import argparse

    # Parse command line arguments
    parser = argparse.ArgumentParser(
        description='Mask R-CNN for nuclei counting and segmentation')
    parser.add_argument("command",
                        metavar="<command>",
                        help="'train' or 'detect'")
    parser.add_argument('--dataset', required=False,
                        metavar="/path/to/dataset/",
                        help='Root directory of the dataset')
    parser.add_argument('--weights', required=True,
                        metavar="/path/to/weights.h5",
                        help="Path to weights .h5 file or 'coco'")
    parser.add_argument('--logs', required=False,
                        default=DEFAULT_LOGS_DIR,
                        metavar="/path/to/logs/",
                        help='Logs and checkpoints directory (default=logs/)')
    parser.add_argument('--subset', required=False,
                        metavar="Dataset sub-directory",
                        help="Subset of dataset to run prediction on")
    args = parser.parse_args()

    # Validate arguments
    if args.command == "train":
        assert args.dataset, "Argument --dataset is required for training"
    elif args.command == "detect":
        assert args.subset, "Provide --subset to run prediction on"

    print("Weights: ", args.weights)
    print("Dataset: ", args.dataset)
    if args.subset:
        print("Subset: ", args.subset)
    print("Logs: ", args.logs)

    # Configurations
    if args.command == "train":
        config = NucleusConfig()
    else:
        config = NucleusInferenceConfig()
    config.display()

    # Create model
    if args.command == "train":
        model = modellib.MaskRCNN(mode="training", config=config,
                                  model_dir=args.logs)
    else:
        model = modellib.MaskRCNN(mode="inference", config=config,
                                  model_dir=args.logs)

    # Select weights file to load
    if args.weights.lower() == "coco":
        weights_path = COCO_WEIGHTS_PATH
        # Download weights file
        if not os.path.exists(weights_path):
            utils.download_trained_weights(weights_path)
    elif args.weights.lower() == "last":
        # Find last trained weights
        weights_path = model.find_last()
    elif args.weights.lower() == "imagenet":
        # Start from ImageNet trained weights
        weights_path = model.get_imagenet_weights()
    else:
        weights_path = args.weights

    # Load weights
    print("Loading weights ", weights_path)
    if args.weights.lower() == "coco":
        # Exclude the last layers because they require a matching
        # number of classes
        model.load_weights(weights_path, by_name=True, exclude=[
            "mrcnn_class_logits", "mrcnn_bbox_fc",
            "mrcnn_bbox", "mrcnn_mask"])
    else:
        model.load_weights(weights_path, by_name=True)


    # Train or evaluate
    if args.command == "train":
        train(model, args.dataset, args.subset)
    elif args.command == "detect":
        detect(model, args.dataset, args.subset)
    else:
        print("'{}' is not recognized. "
              "Use 'train' or 'detect'".format(args.command))
