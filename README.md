UM Biomarkers Group MASK-RCNN for Covid 19 Segmentation

Based on Mask R-CNN availible at https://github.com/matterport/Mask_RCNN

Original credits present in the covid.py file, edits made by Adrian L. Breto.

To use, download the original matterport distribution of Mask-RCNN and use the "weights" folder as your weights argument, running the code on covid.py. See comments for more info.